class Student

  def initialize(first_name , last_name)
    @first_name = first_name
    @last_name = last_name
    @course_load = []
  end

  attr_accessor :first_name, :last_name

  def name
    [@first_name, @last_name].join(" ")
  end

  def courses
    @course_load
  end

  def enroll (new_course)
    return if self.courses.include?(new_course)
    conflict = false
    self.courses.each do |old_course|
      conflict = true if old_course.conflicts_with?(new_course)
    end
    if conflict == false
      new_course.students << self
      self.courses << new_course
    else
      raise Error.new("Conflicts with existing course")
    end
  end

  def course_load
    course_load = Hash.new(0)
    courses.each do |course|
      course_load[course.department] += course.credits
    end
    course_load
  end

end
